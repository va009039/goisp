// lpc11isp.go 2016/3/20

package main

import (
	"flag"
	"fmt"
	"github.com/tarm/serial"
	"io/ioutil"
	"regexp"
)

const (
	FlashSize  = 1024 * 32
	RAM        = 0x10000400
	ChunkSize  = 256
	SectorSize = 4096
)

type target struct {
	p *serial.Port
}

func newTarget(port string, baud int) *target {
	c := &serial.Config{Name: port, Baud: baud}
	s, err := serial.OpenPort(c)
	if err != nil {
		panic(err)
	}
	return &target{p: s}
}

func (t *target) send(str string) {
	t.p.Write([]byte(str))
}

func (t *target) recv(n int) []byte {
	buf := make([]byte, n)
	for i := 0; i < n; {
		r, err := t.p.Read(buf[i:n])
		if err != nil {
			panic(err)
		}
		i += r
	}
	return buf
}

func (t *target) sendln(line string) {
	fmt.Printf("send: %s<CR><LF>\n", line)
	t.send(line + "\r\n")
}

func (t *target) recvln() string {
	fmt.Printf("recv: ")
	s := ""
	for {
		buf := t.recv(1)
		switch c := buf[0]; c {
		case '\r':
			fmt.Printf("<CR>")
		case '\n':
			fmt.Printf("<LF>\n")
			return s
		default:
			fmt.Printf("%c", c)
			s += string(c)
		}
	}
}

func (t *target) waitln(match string) {
	for {
		matched, _ := regexp.MatchString(match, t.recvln())
		if matched {
			break
		}
	}
}

func (t *target) cmd(format string, v ...interface{}) {
	t.sendln(fmt.Sprintf(format, v...))
	t.waitln("^0$") // CMD_SUCCESS
}

func (t *target) sync() {
	fmt.Printf("send: ?\n")
	t.send("?")
	t.waitln("Synchronized$")
	t.sendln("Synchronized")
	t.waitln("OK$")
	t.sendln("12284")
	t.waitln("OK$")
	t.sendln("A 0") // echo off
	t.waitln("0$")
}

func uuencode(src []byte) string {
	dst := ""
	if len(src) > 0 {
		const table = "`!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
		dst = string(table[len(src)])
		for i := 0; i < len(src); i += 3 {
			dst += string(table[src[i]>>2])
			if i+1 >= len(src) {
				dst += string(table[(src[i]&0x03)<<4])
			} else {
				dst += string(table[(src[i]&0x03)<<4|(src[i+1]&0xf0)>>4])
				if i+2 >= len(src) {
					dst += string(table[(src[i+1]&0x0f)<<2])
				} else {
					dst += string(table[(src[i+1]&0x0f)<<2|(src[i+2]&0xc0)>>6])
					dst += string(table[src[i+2]&0x3f])
				}
			}
		}
	}
	return dst
}

func (t *target) toRam(data []byte) {
	t.cmd("W %d %d", RAM, ChunkSize) // write to ram
	cksum := 0
	line := 0
	buf := make([]byte, ChunkSize)
	copy(buf, data)
	for i := 0; i < len(buf); i += 45 {
		j := i + 45
		if j >= len(buf) {
			j = len(buf)
		}
		s := uuencode(buf[i:j])
		t.sendln(s)
		for _, c := range buf[i:j] {
			cksum += int(c)
		}
		line++
		if line >= 20 || j == len(buf) {
			t.sendln(fmt.Sprintf("%d", cksum))
			t.waitln("OK")
			cksum = 0
			line = 0
		}
	}
}

func (t *target) flashWrite(data []byte) {
	t.sync()
	t.cmd("U 23130") // unlock
	sector := 0
	for addr := 0; addr < len(data); addr += ChunkSize {
		t.toRam(data[addr : addr+ChunkSize])
		if (addr % SectorSize) == 0 {
			sector = addr / SectorSize
			t.cmd("P %d %d", sector, sector) // prepare sector
			t.cmd("E %d %d", sector, sector) // erase sector
		}
		t.cmd("P %d %d", sector, sector)
		t.cmd("C %d %d %d", addr, RAM, ChunkSize) // copy ram to flash
	}
}

func main() {
	pPort := flag.String("port", "COM1", "serial port")
	pBaud := flag.Int("baud", 9600, "serial baud")
	flag.Parse()
	filename := flag.Args()[0]
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	fmt.Printf("binfile: %s, size: %d bytes\n", filename, len(data))

	lpc1114 := newTarget(*pPort, *pBaud)
	lpc1114.flashWrite(data)

	fmt.Println("done!")
}
