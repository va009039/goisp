// lpc82isp.go 2016/3/17

package main

import (
	"flag"
	"fmt"
	"github.com/tarm/serial"
	"io/ioutil"
)

const (
	FlashSize = 1024 * 32
	RAM       = 0x10000300
	ChunkSize = 64
)

type target struct {
	p *serial.Port
}

func newTarget(port string, baud int) *target {
	c := &serial.Config{Name: port, Baud: baud}
	s, err := serial.OpenPort(c)
	if err != nil {
		panic(err)
	}
	return &target{p: s}
}

func (t *target) send(str string) {
	t.p.Write([]byte(str))
}

func (t *target) recv(n int) []byte {
	buf := make([]byte, n)
	i := 0
	for i < n {
		r, err := t.p.Read(buf[i:n])
		if err != nil {
			panic(err)
		}
		i += r
	}
	return buf
}

func (t *target) sendln(line string) {
	fmt.Printf("send: %s<CR><LF>\n", line)
	t.send(line + "\r\n")
}

func (t *target) recvln() string {
	fmt.Printf("recv: ")
	s := ""
	for {
		buf := t.recv(1)
		switch c := buf[0]; c {
		case '\r':
			fmt.Printf("<CR>")
		case '\n':
			fmt.Printf("<LF>\n")
			return s
		default:
			fmt.Printf("%c", c)
			s += string(c)
		}
	}
}

func (t *target) waitln(line string) {
	for t.recvln() != line {
	}
}

func (t *target) cmd(format string, v ...interface{}) {
	t.sendln(fmt.Sprintf(format, v...))
	t.waitln("0") // CMD_SUCCESS
}

func (t *target) sync() {
	fmt.Printf("send: ?\n")
	t.send("?")
	t.waitln("Synchronized")
	t.sendln("Synchronized")
	t.waitln("OK")
	t.sendln("12284")
	t.waitln("OK")
	t.cmd("A 0") // echo off
}

func (t *target) toRam(data []byte) {
	t.cmd("W %d %d", RAM, ChunkSize) // write to ram
	buf := make([]byte, ChunkSize)
	copy(buf, data)
	t.send(string(buf))
}

func (t *target) flashWrite(data []byte) {
	t.sync()
	t.cmd("U 23130") // unlock
	sector := 0
	for addr := 0; addr < len(data); addr += ChunkSize {
		t.toRam(data[addr : addr+ChunkSize])
		if (addr % 1024) == 0 {
			sector = addr / 1024
			t.cmd("P %d %d", sector, sector) // prepare sector
			t.cmd("E %d %d", sector, sector) // erase sector
		}
		t.cmd("P %d %d", sector, sector)
		t.cmd("C %d %d %d", addr, RAM, ChunkSize) // copy ram to flash
	}
}

func (t *target) flashVerify(data []byte) {
	for addr := 0; addr < len(data); addr += ChunkSize {
		t.cmd("R %d %d", addr, ChunkSize) // read memory
		r := t.recv(ChunkSize)
		for i := 0; i < ChunkSize && (addr+i) < len(data); i++ {
			if data[addr+i] != r[i] {
				panic("verify error")
			}
		}
	}
}

func main() {
	pPort := flag.String("port", "COM1", "serial port")
	pBaud := flag.Int("baud", 9600, "serial baud")
	flag.Parse()

	filename := flag.Args()[0]
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	fmt.Printf("binfile: %s, size: %d bytes\n", filename, len(data))

	lpc810 := newTarget(*pPort, *pBaud)
	lpc810.flashWrite(data)
	lpc810.flashVerify(data)

	fmt.Printf("Done.\n")
}
